# lib/thorin/chain.rb

module Thorin

  module Chain
    def add_chain_link(link, options = {})
      klass = ( options[:class_name] || link ).camelize.constantize
      class << self
        self.send(:define_method, "chain_#{link}") do
          klass
        end
      end
    end
  
    def get_chain_link(link)
      if self.respond_to?(:"chain_#{link}")
        self.send(:"chain_#{link}" )
      else
        link.camelize.constantize
      end
    end
  end
end