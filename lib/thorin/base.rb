class Thorin::Base

  def initialize(app)  
    @app                          = app  
  end  
    
  def call(env)  
    request          = Rack::Request.new(env)
    params           = Rack::Utils.parse_nested_query(request.query_string).symbolize_keys

    
    ( status, 
      headers, 
      @response )     = @app.call(env)  
   
    # Should iterate over the other path elements to handle chaining.
    ( element, 
      params[:id], 
      collection )   = request.path.match(/.*\/(\w+)\/(\d+)$|.*\/(\w+)$/).captures
    
    collection       = collection.camelize.constantize if collection
    element          = element.camelize.constantize    if element
    
    # Put the parent ids in. 
    chains = request.path.scan(/\/(\w+)\/(\d+)|\/(\w+)/).map do |elem, id, coll| 
                if elem && id
                  params[:"#{elem}_id" ] = id unless elem == element
                  elem                         
                else
                  coll
                end
              end.reverse.inject([]) do |h,i|
                h <<  if h.count == 0
                        i.camelize.constantize
                      else
                        h.last.get_chain_link(i)
                      end
              end
      
    
    @message = if collection
                collection.send(:"collection_#{request.request_method.downcase}", params)
              elsif element
                element.send(:"element_#{request.request_method.downcase}", params)
              end
              
    fmt     = [ [ headers["Content-Type"] ].flatten.map do |h|
                m = Thorin::Format.format[h]
                if ( collection || @message ).try(:respond_to?, m )
                  m
                end
              end ].flatten.compact.first
    
    # Grab the chains
    @message = chains.inject(@message) do |m, link|
                link.call_link(params, @message) if link.respond_to?(:call_link)
              end
    
    # Pass it to the renderer.
    @message = if fmt && collection
                collection.send(fmt, @message)
              elsif fmt
                @message.send(fmt)
              else
                "<p>Error #{fmt} not supported for #{@headers['Content-Type']}</p>\n"
              end
    

    [status, headers, self]  
  end  
    
  def each(&block) 
    block.call(@message) 
    @response.each(&block)  
  end
  
end