
module Thorin
  module ActiveRecord
    
    def self.collection_get(params)
      self.where(params)
    end
    
    def self.element_get(params)
      self.collection_get.first
    end
  end
end
