class Thorin::Format
  def self.register(content_type, fmt)
    @format ||= {}
    @format["#{content_type}"] = :"to_#{fmt}"
  end
  
  def self.format
    @format
  end
  
end