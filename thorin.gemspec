$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "thorin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "thorin"
  s.version     = Thorin::VERSION
  s.authors     = ['Jason Kenney']
  s.email       = ['bloodycelt@gmail.com']
  s.homepage    = "https://github.com/bloodycelt"
  s.summary     = "Thorin a Ruby Framework aimed at quickly deploying web services."
  s.description = "Thorin - at your service."

  s.files = [
    'MIT-LICENSE', 
    'Rakefile',
    'README.md',
    'lib/thorin.rb',
    'lib/thorin/active_record.rb',
    'lib/thorin/chain.rb',
    'lib/thorin/format.rb'
  ]
  
  s.test_files = [
    'spec/thorin_spec.rb',
    'spec/spec_helper.rb'
  ]

  s.add_dependency 'active_support'
  s.add_dependency 'rack'
  
  s.add_development_dependency 'activerecord'
  s.add_development_dependency 'factory_girl'
  s.add_development_dependency 'rack-test'
  s.add_development_dependency 'rspec'
  s.add_development_dependency 'sqlite3'
end
