require 'spec_helper'
require 'rack/test'
require 'thorin'

describe Thorin do
  include Rack::Test::Methods
  
  let(:inner_app) do
    lambda { |env| [200, {'Content-Type' => 'application/json'}, [] ] }
  end

  let(:app) { Thorin::Base.new(inner_app) }
  
  it "calls grog alone" do
    response = get "/grog/19"
    last_response.should be_ok
    response.body.should eq( { 'response' => 1 }.to_json )
  end
  
  it "calls grog and mog" do
    response = get "/mog/grog/19"
    last_response.should be_ok
    response.body.should eq( { 'response' => 2 }.to_json )
  end
  
  it "posts to grog" do
    response = post "/grog/19?response=3"
    last_response.should be_ok
    response.body.should eq( { 'status' => 'ok' }.to_json )
  end
end

class Grog
  extend Thorin::Chain
  
  def initialize(attributes = {})
    super()
  end
  
  def response
    @response
  end
  
  def response=(value)
    @response = value
  end
  
  def self.element_get(params)
    g = Grog.new
    g.response = 1
    g
  end
  
  def self.element_post(params)
    g = Grog.new
    g.response = params[:response]
    { :status => 'ok' }  
  end
  
  def self.collection_get(params)
    g = Grog.new
    g.response = "<p> #{self.name}: COLLECTION GET #{params}</p>\n"
    [ g ]
  end
  
  def to_json
    { 'response' => @response }.to_json
  end
end

class Mog < Grog
  
  def self.call_link(params, response)
    response.response = 2
    response
  end
  
end

Thorin::Format.register 'text/html', :html
Thorin::Format.register 'application/json', :json