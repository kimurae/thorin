# Thorin

A web services framework in Ruby. The goal of this framework is the following:

1. Be able to install a base scaffolding like sinatra and rails that will let you
immediately get the 'service' service that reflects all services and their attributes. This would
allow something like angular or backbone to use that for building models dynamically.

2. Have an automatic routing and dispatch that matches the url to a service and assumes there
is a class with that name, if it cannot find such a class it should return a 404.

3. Support Catalyst style chaining to allow filters and chained events to kick off based on
the url. These should also make the assumption that the chain name is the name of the class.

4. Add in overrides to these assumptions, after all we can't be too opinionated.

Still under construction, but any suggestions are welcome ^_^

And no, this project isn't dead. I just have a product release at my real job :( .
